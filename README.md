# Introduction

This repo contains standard CI scripts for building docker images, doing tests and creating helm packages (according to https://docs.gitlab.com/ee/ci/yaml/includes.html)

For usage just create an standard git-ci yml and insert the include statements to the yaml from this folder.

# Usage


```
include:
- project: 'eclipse/xfsc/dev-ops/ci-templates'  
  file: 'golang-standard-ci.yaml'  
  ref: main

```

# Developer Information

## Helm

Helm will be generated automatically and pushed to harbor. "-tag" is replaced later on by the commit tag/branch if used.

## Golang

Creates an go build and accepts docker args.

## Maven

Standard maven pipeline for java packages.

## Docker 

Standard Docker pipeline which creates images over [build kit](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-runner-service-documentation#build-container-image-buildkit). If no commit tag is available, the default branch name is used as latest, otherwise the tag or branchname by using branch-{name} as defined in [eclipse templates](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates/-/blob/main/jobs/buildkit.gitlab-ci.yml?ref_type=heads#L17) 

Docker file is automatically pushed to harbor so far the integration is configured.

# Gitlab Runner

## Installation of Docker Executor based Runner

To install a gitlab runner the following steps should be followed (after the docker-ce installation): 

1. Pull Docker Image 
```
docker pull gitlab/gitlab-runner
```

2. Run Docker 
```
docker run -d --name gitlab-runner --restart always \
    -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    gitlab/gitlab-runner
```

3. Register the Runner 

```
docker exec -it gitlab-runner gitlab-runner register --url https://gitlab.eclipse.org --token XY
```

## Configuration
The gitlab runner can be configured by using the tags of the [eclipse runner](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-runner-service-documentation#target-a-specific-runner). Currently the standard pipelines are configured to the tags [origin:eclipse](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-runner-service-documentation#first-integration) and ctx:medium for a [medium runner](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-runner-service-documentation#resource-pack-configuration). Small is also available if this fits to the needs. 

NOTE: The eclipse runner supports no kaniko docker builds in cause of security reasons. This means [build kit](https://github.com/moby/buildkit) must be used as [build action](https://gitlab.eclipse.org/eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates/-/blob/main/jobs/buildkit.gitlab-ci.yml?ref_type=heads#L49): 

```
include:
  - project: 'eclipsefdn/it/releng/gitlab-runner-service/gitlab-ci-templates'
    file: '/jobs/buildkit.gitlab-ci.yml'


docker-build:
  extends: .buildkit
  stage: docker
  variables:
    CI_REGISTRY: ${HARBOR_HOST}
    CI_REGISTRY_USER: ${HARBOR_USERNAME}
    CI_REGISTRY_PASSWORD: ${HARBOR_PASSWORD}
    CI_REGISTRY_IMAGE: ${HARBOR_HOST}/${HARBOR_PROJECT}/${CI_PROJECT_NAME}
    DOCKERFILE_NAME: deployment/docker/Dockerfile
    BUILD_ARG: $[[ inputs.docker_args ]]
```

If build args are defined in the ci they must be change like this: 

```
  docker_args: --build-arg pluginRepoUrl=${vdrPluginRepoUrl} --build-arg pluginTag=${tag}


  docker_args:  --opt build-arg:pluginRepoUrl=${vdrPluginRepoUrl} --opt build-arg:pluginTag=${tag}
```

When there is any error like: 

ERROR: Job failed (system failure): prepare environment: setting up build pod: pods "runner-t2yke2-j-project-5741-concurrent-0-fw4l5y6d" is forbidden: exceeded quota: grac-xfsc-rq, requested: limits.cpu=2150m,limits.memory=5Gi,requests.cpu=1100m,requests.memory=5Gi, used: limits.cpu=4700m,limits.memory=10752Mi,requests.cpu=2400m,requests.memory=10496Mi, limited: limits.cpu=5100m,limits.memory=12Gi,requests.cpu=2750m,requests.memory=12032Mi. Check https://docs.gitlab.com/runner/shells/index.html#shell-profile-loading for more information

Restart the job:) 

